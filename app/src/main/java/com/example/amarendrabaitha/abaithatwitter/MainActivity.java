package com.example.amarendrabaitha.abaithatwitter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;

public class MainActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "oMyMYy7EZhICWy7WFVyCTeJgc";
    private static final String TWITTER_SECRET = "cwXtjZFaJ7D15CJfly77HS3pe9ync2DoaZEOHaXvM5I3kJvsHH";

    TwitterLoginButton loginButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_main);

        loginButton=(TwitterLoginButton)findViewById(R.id.Login_button);
        final ImageView imgProfile=(ImageView)findViewById(R.id.imgProfile);
        final TextView txtDetails=(TextView)findViewById(R.id.txtDetails);

        loginButton.setCallback(new Callback<TwitterSession>()
        {
            @Override
            public void success(Result<TwitterSession> result)
            {
                Toast.makeText(getApplication(),"hello from button click",Toast.LENGTH_LONG).show();
                TwitterSession session=result.data;
                final String userName=result.data.getUserName();
                Call<User> user= Twitter.getApiClient(session).getAccountService().verifyCredentials(true,false);

                user.enqueue(new Callback<User>() {
                    @Override
                    public void success(Result<User> result)
                    {

                        User userInfo=result.data;
                        String email=userInfo.email;
                        String description=userInfo.description;
                        String location=userInfo.location;
                        int friendsCount=userInfo.friendsCount;
                        int favouritesCount=userInfo.favouritesCount;
                        int followersCount=userInfo.followersCount;

                        // for Image loading
                        String profileImageUrl =userInfo.profileImageUrl.replace("_normal","");
                        Picasso.with(getApplicationContext()).load(profileImageUrl).into(imgProfile);

                        // for personal information show

                        StringBuilder sb=new StringBuilder();
                        sb.append("User name: "+userName);
                        sb.append("\n");
                        sb.append("Email id: "+email);
                        sb.append("\n");
                        sb.append("Description: "+description);
                        sb.append("\n");
                        sb.append("Location: "+location);
                        sb.append("\n");
                        sb.append("Total no of friends :"+friendsCount);
                        sb.append("\n");
                        sb.append("favouritesCount: "+favouritesCount);
                        sb.append("\n");
                        sb.append("Follower count: "+followersCount);

                        txtDetails.setText(sb.toString());
                        loginButton.setVisibility(View.INVISIBLE);


                    }

                    @Override
                    public void failure(TwitterException exception)
                    {
                        Toast.makeText(getApplication(),"error in connection", Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @Override
            public void failure(TwitterException exception)
            {
                Log.e("some error in conn",exception.getMessage());
            }
        });

    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the login button.
        loginButton.onActivityResult(requestCode, resultCode, data);
    }
}



